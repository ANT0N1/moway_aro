
// MiMowayDlg.h: archivo de encabezado
//

#pragma once


// Cuadro de diálogo de CMiMowayDlg
class CMiMowayDlg : public CDialogEx
{
// Construcción
public:
	CMiMowayDlg(CWnd* pParent = nullptr);	// Constructor estándar

// Datos del cuadro de diálogo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MIMOWAY_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementación
protected:
	HICON m_hIcon;

	// Funciones de asignación de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBotonencender();
	afx_msg void OnBnClickedApagar();
	afx_msg void OnBnClickedLed1();
	afx_msg void OnBnClickedLed2();
	afx_msg void OnBnClickedLed3();
	afx_msg void OnBnClickedLed4();
	void desconectarMoway();
	void cambiarVelocidadMotores(int izquierdo, int derecho);
	afx_msg void OnBnClickedAvanzar();
	afx_msg void OnBnClickedParar();
	afx_msg void OnBnClickedRetroceder();
	afx_msg void OnBnClickedIzquierda();
	afx_msg void OnBnClickedDerecha();
	CSliderCtrl sliderIzquierdo;
	afx_msg void OnNMReleasedcaptureSliderIzquierdo(NMHDR *pNMHDR, LRESULT *pResult);
	CSliderCtrl sliderDerecho;
	afx_msg void OnNMReleasedcaptureSliderDerecho(NMHDR *pNMHDR, LRESULT *pResult);
	void OnDestroy();
	static UINT ThreadLuz(LPVOID pParam);
	void CheckResetearVecesPalmada();
	CWinThread *m_ThreadSensores;
	afx_msg void OnBnClickedApagarLeds();
	void mensajeRobotNoConectado();
	CButton apagarLeds;
	CProgressCtrl progressLuz;
	CProgressCtrl progressBateria;
	CProgressCtrl progressLS;
	CProgressCtrl progressCLS;
	CProgressCtrl progressCRS;
	CProgressCtrl progressRS;
	CButton led4;
	CButton led3;
	CButton led2;
	CButton led1;
	CProgressCtrl progressAcelX;
	CProgressCtrl progressAcelY;
	CProgressCtrl progressAcelZ;
	CButton labelTemperatura;
	CProgressCtrl progressRuido;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
