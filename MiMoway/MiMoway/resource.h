//{{NO_DEPENDENCIES}}
// Archivo de inclusi�n generado de Microsoft Visual C++.
// Usado por MiMoway.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MIMOWAY_DIALOG              102
#define IDR_MAINFRAME                   128
#define btEncender                      1000
#define btApagar                        1001
#define IDC_BUTTON1                     1002
#define IDC_ENCENDER                    1002
#define IDC_BOTONENCENDER               1002
#define IDC_APAGAR                      1003
#define IDC_LED1                        1004
#define IDC_LED2                        1005
#define IDC_LED3                        1006
#define IDC_LED4                        1007
#define IDC_AVANZAR                     1008
#define IDC_PARAR                       1009
#define IDC_RETROCEDER                  1010
#define IDC_IZQUIERDA                   1011
#define IDC_DERECHA                     1012
#define IDC_SLIDERIZQUIERDA             1014
#define IDC_SLIDER_IZQUIERDO            1014
#define IDC_SLIDER1                     1016
#define IDC_SLIDER_DERECHO              1016
#define IDC_APAGAR_LEDS                 1017
#define IDC_PROGRESS_LUZ                1018
#define IDC_PROGRESS_BATERIA            1019
#define IDC_PROGRESS_LS                 1020
#define IDC_PROGRESS_CLS                1021
#define IDC_PROGRESS_RS                 1022
#define IDC_PROGRESS_CRS                1023
#define IDC_PROGRESS_ACEL_X             1024
#define IDC_PROGRESS_ACEL_Y             1025
#define IDC_PROGRESS_ACEL_Z             1026
#define IDC_STATIC_TEMPERATURA          1027
#define IDC_PROGRESS5                   1029
#define IDC_PROGRESS_RUIDO              1029
#define IDC_STATIC_RUIDO                1030

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1031
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
