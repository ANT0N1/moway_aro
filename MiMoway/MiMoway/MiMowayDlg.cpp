
// MiMowayDlg.cpp: archivo de implementación
//

#include "pch.h"
#include "framework.h"
#include "MiMoway.h"
#include "MiMowayDlg.h"
#include "afxdialogex.h"
#include "CMoway.h"
#include "string.h"
#include <iostream>
#include <string>
#include <time.h>
#include <stdio.h>

CMoway mimoway;
int MOWAY_ID = 21;
int velocidadMotorIzquierdo = 0;
int direccionMotorIzquierdo = CMoway::FORWARD;
int velocidadMotorDerecho = 0;
int direccionMotorDerecho = CMoway::FORWARD;

int luz = 0;
int bateria = 0;
int lS = 0, clS = 0, crS = 0, rS = 0;
int aceleracionX = 0, aceleracionY = 0, aceleracionZ = 0;
int temperatura = 0;
int ruido = 0;
int veces = 0;
bool enMovimientoPalmada = false;
bool robotConectado = false;
int iteracionesThread = 0;
int ultimaIteracionReseteo = 0;



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cuadro de diálogo CAboutDlg utilizado para el comando Acerca de

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();


	// Datos del cuadro de diálogo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV

// Implementación
protected:
	DECLARE_MESSAGE_MAP()
public:

};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cuadro de diálogo de CMiMowayDlg



CMiMowayDlg::CMiMowayDlg(CWnd* pParent /*= nullptr*/)
	: CDialogEx(IDD_MIMOWAY_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMiMowayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_IZQUIERDO, sliderIzquierdo);
	DDX_Control(pDX, IDC_SLIDER_DERECHO, sliderDerecho);
	DDX_Control(pDX, IDC_APAGAR_LEDS, apagarLeds);
	DDX_Control(pDX, IDC_PROGRESS_LUZ, progressLuz);
	DDX_Control(pDX, IDC_PROGRESS_BATERIA, progressBateria);
	DDX_Control(pDX, IDC_PROGRESS_LS, progressLS);
	DDX_Control(pDX, IDC_PROGRESS_CLS, progressCLS);
	DDX_Control(pDX, IDC_PROGRESS_CRS, progressCRS);
	DDX_Control(pDX, IDC_PROGRESS_RS, progressRS);
	DDX_Control(pDX, IDC_LED4, led4);
	DDX_Control(pDX, IDC_LED3, led3);
	DDX_Control(pDX, IDC_LED2, led2);
	DDX_Control(pDX, IDC_LED1, led1);
	DDX_Control(pDX, IDC_PROGRESS_ACEL_X, progressAcelX);
	DDX_Control(pDX, IDC_PROGRESS_ACEL_Y, progressAcelY);
	DDX_Control(pDX, IDC_PROGRESS_ACEL_Z, progressAcelZ);
	DDX_Control(pDX, IDC_APAGAR, labelTemperatura);
	DDX_Control(pDX, IDC_PROGRESS_RUIDO, progressRuido);
}

BEGIN_MESSAGE_MAP(CMiMowayDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BOTONENCENDER, &CMiMowayDlg::OnBnClickedBotonencender)
	ON_BN_CLICKED(IDC_APAGAR, &CMiMowayDlg::OnBnClickedApagar)
	ON_BN_CLICKED(IDC_LED1, &CMiMowayDlg::OnBnClickedLed1)
	ON_BN_CLICKED(IDC_LED2, &CMiMowayDlg::OnBnClickedLed2)
	ON_BN_CLICKED(IDC_LED3, &CMiMowayDlg::OnBnClickedLed3)
	ON_BN_CLICKED(IDC_LED4, &CMiMowayDlg::OnBnClickedLed4)
	ON_BN_CLICKED(IDC_AVANZAR, &CMiMowayDlg::OnBnClickedAvanzar)
	ON_BN_CLICKED(IDC_PARAR, &CMiMowayDlg::OnBnClickedParar)
	ON_BN_CLICKED(IDC_RETROCEDER, &CMiMowayDlg::OnBnClickedRetroceder)
	ON_BN_CLICKED(IDC_IZQUIERDA, &CMiMowayDlg::OnBnClickedIzquierda)
	ON_BN_CLICKED(IDC_DERECHA, &CMiMowayDlg::OnBnClickedDerecha)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_IZQUIERDO, &CMiMowayDlg::OnNMReleasedcaptureSliderIzquierdo)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_DERECHO, &CMiMowayDlg::OnNMReleasedcaptureSliderDerecho)
	ON_BN_CLICKED(IDC_APAGAR_LEDS, &CMiMowayDlg::OnBnClickedApagarLeds)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// Controladores de mensajes de CMiMowayDlg

BOOL CMiMowayDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// Agregar el elemento de menú "Acerca de..." al menú del sistema.
	// IDM_ABOUTBOX debe estar en el intervalo de comandos del sistema.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Establecer el icono para este cuadro de diálogo.  El marco de trabajo realiza esta operación
	//  automáticamente cuando la ventana principal de la aplicación no es un cuadro de diálogo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono pequeño

	// TODO: agregar aquí inicialización adicional

	sliderIzquierdo.SetRange(-100, 100, TRUE);
	sliderDerecho.SetRange(-100, 100, TRUE);
	apagarLeds.SetCheck(true);
	progressLuz.SetRange(0, 100);
	progressBateria.SetRange(0, 100);
	progressCLS.SetRange(0, 255);
	progressCRS.SetRange(0, 255);
	progressLS.SetRange(0, 255);
	progressRS.SetRange(0, 255);
	progressAcelX.SetRange(0, 255);
	progressAcelY.SetRange(0, 255);
	progressAcelZ.SetRange(0, 255);
	CStatic* pStaticTemperatura = (CStatic*)GetDlgItem(IDC_STATIC_TEMPERATURA);
	pStaticTemperatura->SetWindowText(TEXT(""));
	CStatic* pStaticRuido = (CStatic*)GetDlgItem(IDC_STATIC_RUIDO);
	pStaticRuido->SetWindowText(TEXT(""));
	
	progressRuido.SetRange(0, 255);

	m_ThreadSensores = NULL;
	UpdateData(FALSE);
	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

void CMiMowayDlg::OnSysCommand(UINT nID, LPARAM lParam)
{

	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
	if (nID == SC_CLOSE)
	{
		if (robotConectado)
			desconectarMoway();
	}
}

// Si agrega un botón Minimizar al cuadro de diálogo, necesitará el siguiente código
//  para dibujar el icono.  Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operación la realiza automáticamente el marco de trabajo.

void CMiMowayDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rectángulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// El sistema llama a esta función para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CMiMowayDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMiMowayDlg::OnBnClickedBotonencender()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	robotConectado = mimoway.CheckConnected();
	if (!robotConectado) {
		mimoway.ConnectMoway(MOWAY_ID);
		robotConectado = mimoway.CheckConnected();
		if (robotConectado) {
			m_ThreadSensores = AfxBeginThread(ThreadLuz, this, 0); // Leer sensores con Thread (funciona mejor)
			//SetTimer(1, 25, NULL); // Leer sensores con timer
		}
		else
			AfxMessageBox((CString)"Error al conectar el robot. Vuelve a intentarlo");
	}
	else {
		AfxMessageBox((CString)"Robot ya conectado");
	}

	//std::string errores = mimoway.GetErrors();
	//if (errores != "") {
	//	AfxMessageBox((CString)errores.c_str());
	//}
}

void CMiMowayDlg::OnBnClickedApagar()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		desconectarMoway();
	}
	else {
		mensajeRobotNoConectado();
	}

	//std::string errores = mimoway.GetErrors();
	//if (errores != "") {
	//	AfxMessageBox((CString)errores.c_str());
	//}
}

void CMiMowayDlg::OnBnClickedLed1()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.ChangeLEDState(CMoway::LED_FRONT, CMoway::ON);// enciende LED frontal
		mimoway.ChangeLEDState(CMoway::LED_BRAKE, CMoway::OFF);// apaga LED de freno
		mimoway.ChangeLEDState(CMoway::LED_TOP_GREEN, CMoway::OFF);// apaga el LED verde
		mimoway.ChangeLEDState(CMoway::LED_TOP_RED, CMoway::OFF);// apaga el LED rojo
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedLed2()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.ChangeLEDState(CMoway::LED_FRONT, CMoway::OFF);// apaga LED frontal
		mimoway.ChangeLEDState(CMoway::LED_BRAKE, CMoway::ON);// enciende LED de freno
		mimoway.ChangeLEDState(CMoway::LED_TOP_GREEN, CMoway::OFF);// apaga el LED verde
		mimoway.ChangeLEDState(CMoway::LED_TOP_RED, CMoway::OFF);// apaga el LED rojo
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedLed3()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.ChangeLEDState(CMoway::LED_FRONT, CMoway::OFF);// apaga LED frontal
		mimoway.ChangeLEDState(CMoway::LED_BRAKE, CMoway::OFF);// apaga LED de freno
		mimoway.ChangeLEDState(CMoway::LED_TOP_GREEN, CMoway::ON);// enciende el LED verde
		mimoway.ChangeLEDState(CMoway::LED_TOP_RED, CMoway::OFF);// apaga el LED rojo
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedLed4()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.ChangeLEDState(CMoway::LED_FRONT, CMoway::OFF);// apaga LED frontal
		mimoway.ChangeLEDState(CMoway::LED_BRAKE, CMoway::OFF);// apaga LED de freno
		mimoway.ChangeLEDState(CMoway::LED_TOP_GREEN, CMoway::OFF);// apaga el LED verde
		mimoway.ChangeLEDState(CMoway::LED_TOP_RED, CMoway::ON);// enciende el LED rojo
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedApagarLeds()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.ChangeLEDState(CMoway::LED_FRONT, CMoway::OFF);// apaga LED frontal
		mimoway.ChangeLEDState(CMoway::LED_BRAKE, CMoway::OFF);// apaga LED de freno
		mimoway.ChangeLEDState(CMoway::LED_TOP_GREEN, CMoway::OFF);// apaga el LED verde
		mimoway.ChangeLEDState(CMoway::LED_TOP_RED, CMoway::OFF);// apaga el LED rojo
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedAvanzar()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.GoStraight(50, CMoway::FORWARD);
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}

}

void CMiMowayDlg::OnBnClickedParar()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.MotorStop();
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedRetroceder()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.GoStraight(50, CMoway::BACKWARD);
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedIzquierda()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.SetSpeed(25, 25, CMoway::BACKWARD, CMoway::FORWARD);
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnBnClickedDerecha()
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		mimoway.SetSpeed(25, 25, CMoway::FORWARD, CMoway::BACKWARD);
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnNMReleasedcaptureSliderIzquierdo(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Agregue aquí su código de controlador de notificación de control

	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		int motorIzquierdo = (sliderIzquierdo.GetPos())*(-1);
		cambiarVelocidadMotores(motorIzquierdo, NULL);
		m_ThreadSensores->ResumeThread();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::OnNMReleasedcaptureSliderDerecho(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Agregue aquí su código de controlador de notificación de control
	if (robotConectado) {
		m_ThreadSensores->SuspendThread();
		int motorDerecho = (sliderDerecho.GetPos())*(-1);
		cambiarVelocidadMotores(NULL, motorDerecho);
		m_ThreadSensores->ResumeThread();

	}
	else {
		mensajeRobotNoConectado();
	}

}



void CMiMowayDlg::OnDestroy() {
	if (robotConectado) {
		desconectarMoway();
	}

}

UINT CMiMowayDlg::ThreadLuz(LPVOID pParam)
{
	while (1) {
		iteracionesThread++;
		robotConectado = mimoway.CheckConnected();
		if (robotConectado) {
			CMiMowayDlg *pMisDatos = (CMiMowayDlg *)pParam;
			CStatic* pStaticTemperatura = (CStatic*)pMisDatos->GetDlgItem(IDC_STATIC_TEMPERATURA);
			mimoway.ReadMicSensor(&ruido);
			pMisDatos->progressRuido.SetPos(ruido);
			CStatic* pStaticRuido = (CStatic*)pMisDatos->GetDlgItem(IDC_STATIC_RUIDO);
			CString tempStringRuido;
			tempStringRuido.Format(_T("%d"), ruido);
			pStaticRuido->SetWindowText(tempStringRuido);
			mimoway.ReadAmbientLightSensor(&luz);
			pMisDatos->progressLuz.SetPos(luz);
			mimoway.ReadBatteryStatus(&bateria);
			pMisDatos->progressBateria.SetPos(bateria);
			mimoway.ReadProximitySensors(&lS, &clS, &crS, &rS);
			pMisDatos->progressCLS.SetPos(clS);
			pMisDatos->progressCRS.SetPos(crS);
			pMisDatos->progressLS.SetPos(lS);
			pMisDatos->progressRS.SetPos(rS);
			mimoway.ReadAccelerator(&aceleracionX, CMoway::X);
			mimoway.ReadAccelerator(&aceleracionY, CMoway::Y);
			mimoway.ReadAccelerator(&aceleracionZ, CMoway::Z);
			pMisDatos->progressAcelX.SetPos(aceleracionX);
			pMisDatos->progressAcelY.SetPos(aceleracionY);
			pMisDatos->progressAcelZ.SetPos(aceleracionZ);
			mimoway.ReadTemp(&temperatura);
			CString tempString;
			tempString.Format(_T("%d Grados"), temperatura);
			pStaticTemperatura->SetWindowText(tempString);

			 // Mover con palmadas comprobando el seonsor de forma digital
			/*int ruidoDigital;
			mimoway.CheckMicSensor(&ruidoDigital);

			if (ruidoDigital && !enMovimientoPalmada) {
				mimoway.GoStraight(50, CMoway::FORWARD);
				enMovimientoPalmada = true;
			}

			if (ruidoDigital && enMovimientoPalmada) {
				veces++;
				if (veces == 2) {
					mimoway.MotorStop();
					veces = 0;
				}
			}*/

			
			// Mover con palmadas comprobando el seonsor de forma analógica suponiendo un ruido mayor del 50% es una palmada
			if (ruido > 255 * 0.5 && !enMovimientoPalmada) {
				mimoway.GoStraight(50, CMoway::FORWARD);
			}
			if (ruido > 255 * 0.5 && enMovimientoPalmada) {
				veces++;
				if (veces == 2) {
					mimoway.MotorStop();
					veces = 0;
				}
			}

			pMisDatos->CheckResetearVecesPalmada();
		}
		Sleep(10);
	}
	AfxEndThread(1);
	return 0;
}

void CMiMowayDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Agregue aquí su código de controlador de mensajes o llame al valor predeterminado
	if (robotConectado) {
		CStatic* pStaticTemperatura = (CStatic*)GetDlgItem(IDC_STATIC_TEMPERATURA);
		mimoway.ReadMicSensor(&ruido);
		progressRuido.SetPos(ruido);
		mimoway.ReadAmbientLightSensor(&luz);
		progressLuz.SetPos(luz);
		mimoway.ReadBatteryStatus(&bateria);
		progressBateria.SetPos(bateria);
		mimoway.ReadProximitySensors(&lS, &clS, &crS, &rS);
		progressCLS.SetPos(clS);
		progressCRS.SetPos(crS);
		progressLS.SetPos(lS);
		progressRS.SetPos(rS);
		mimoway.ReadAccelerator(&aceleracionX, CMoway::X);
		mimoway.ReadAccelerator(&aceleracionY, CMoway::Y);
		mimoway.ReadAccelerator(&aceleracionZ, CMoway::Z);
		progressAcelX.SetPos(aceleracionX);
		progressAcelY.SetPos(aceleracionY);
		progressAcelZ.SetPos(aceleracionZ);
		mimoway.ReadTemp(&temperatura);
		CString tempString;
		tempString.Format(_T("%d Grados"), temperatura);
		pStaticTemperatura->SetWindowText(tempString);
		CStatic* pStaticRuido = (CStatic*)GetDlgItem(IDC_STATIC_RUIDO);
		CString tempStringRuido;
		tempStringRuido.Format(_T("%d"), ruido);
		pStaticRuido->SetWindowText(tempStringRuido);

		// Mover con palmadas comprobando el seonsor de forma digital
	   /*int ruidoDigital;
	   mimoway.CheckMicSensor(&ruidoDigital);

	   if (ruidoDigital && !enMovimientoPalmada) {
		   mimoway.GoStraight(50, CMoway::FORWARD);
		   enMovimientoPalmada = true;
	   }

	   if (ruidoDigital && enMovimientoPalmada) {
		   veces++;
		   if (veces == 2) {
			   mimoway.MotorStop();
			   veces = 0;
		   }
	   }*/


	   // Mover con palmadas comprobando el seonsor de forma analógica suponiendo un ruido mayor del 50% es una palmada
		if (ruido > 255 * 0.5 && !enMovimientoPalmada) {
			mimoway.GoStraight(50, CMoway::FORWARD);
		}
		if (ruido > 255 * 0.5 && enMovimientoPalmada) {
			veces++;
			if (veces == 2) {
				mimoway.MotorStop();
				veces = 0;
			}
		}

		CheckResetearVecesPalmada();
	}
	Sleep(50);
	CDialogEx::OnTimer(nIDEvent);
}

void CMiMowayDlg::cambiarVelocidadMotores(int izquierdo, int derecho)
{
	if (robotConectado) {
		if (izquierdo != NULL && izquierdo >= -100 && izquierdo <= 100) {
			velocidadMotorIzquierdo = abs(izquierdo);
			if (izquierdo < 0) {
				direccionMotorIzquierdo = CMoway::BACKWARD;
			}
			else {
				direccionMotorIzquierdo = CMoway::FORWARD;
			}
		}
		if (derecho != NULL && derecho >= -100 && derecho <= 100) {
			velocidadMotorDerecho = abs(derecho);
			if (derecho < 0) {
				direccionMotorDerecho = CMoway::BACKWARD;
			}
			else {
				direccionMotorDerecho = CMoway::FORWARD;
			}
		}


		mimoway.SetSpeed(velocidadMotorIzquierdo, velocidadMotorDerecho, (CMoway::direction)direccionMotorIzquierdo, (CMoway::direction)direccionMotorDerecho);
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::CheckResetearVecesPalmada() {
	if (iteracionesThread - ultimaIteracionReseteo >= 10) {
		veces = 0;
		ultimaIteracionReseteo = iteracionesThread;
	}
}

void CMiMowayDlg::desconectarMoway()
{
	//m_ThreadLuz->SuspendThread();
	if (robotConectado) {
		::TerminateThread(m_ThreadSensores, 0);
		//KillTimer(1);
		mimoway.MotorStop();
		mimoway.ChangeLEDState(CMoway::LED_FRONT, CMoway::OFF);// apaga LED frontal
		mimoway.ChangeLEDState(CMoway::LED_BRAKE, CMoway::OFF);// apaga LED de freno
		mimoway.ChangeLEDState(CMoway::LED_TOP_GREEN, CMoway::OFF);// apaga el LED verde
		mimoway.ChangeLEDState(CMoway::LED_TOP_RED, CMoway::OFF);// apaga el LED rojo

		led1.SetCheck(false);
		led2.SetCheck(false);
		led3.SetCheck(false);
		led4.SetCheck(false);
		apagarLeds.SetCheck(true);

		progressBateria.SetPos(0);
		progressLuz.SetPos(0);
		sliderDerecho.SetPos(0);
		sliderIzquierdo.SetPos(0);

		progressCLS.SetPos(0);
		progressCRS.SetPos(0);
		progressLS.SetPos(0);
		progressRS.SetPos(0);

		progressAcelX.SetPos(0);
		progressAcelY.SetPos(0);
		progressAcelZ.SetPos(0);

		progressRuido.SetPos(0);
		UpdateData();

		mimoway.DisconnectMoway();
	}
	else {
		mensajeRobotNoConectado();
	}
}

void CMiMowayDlg::mensajeRobotNoConectado()
{
	AfxMessageBox((CString)"Error. El robot no está conectado");
}